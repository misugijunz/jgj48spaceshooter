﻿using UnityEngine;
using System.Collections;

public class ObstacleMovement : MonoBehaviour {
	[SerializeField]
	GameObject explosion;
	
	float speed = 1.0f;
	
	void FixedUpdate ()
	{
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag("Player") && other.transform.Find("Display").renderer.enabled)
		{
			Instantiate(explosion, other.transform.position, other.transform.rotation);
			other.transform.Find("Display").renderer.enabled = false;
		}
	}
}