﻿using UnityEngine;
using System.Collections;

public class MouseFollow : MonoBehaviour {
	CharacterController controller;
	Vector3 velocity;
	Vector3 direction;
	
	void Start () 
	{
		controller = GetComponent<CharacterController>();
	}
	
	void FixedUpdate () 
	{
		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
		{
			direction = hit.point - transform.position;
			direction.y = 0.0f;
			controller.Move(direction * Time.deltaTime);
		}
	}
}
