﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	[SerializeField]
	GameObject explosion;
	
	[SerializeField]
	GameObject coin;
	
	float speed = 10.0f;
	
	void FixedUpdate ()
	{
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag("Enemy"))
		{
			Instantiate(explosion, transform.position, transform.rotation);
			float rand = Random.Range(0.0f, 100.0f);
			if (rand > 50.0f)
				Instantiate(coin, transform.position, transform.rotation);
			PlayerStat.score += 100;
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
	}
}
