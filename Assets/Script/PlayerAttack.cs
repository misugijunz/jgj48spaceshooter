﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {
	[SerializeField]
	GameObject bullet;
	
	float shootTimer = 0.0f;
	float shootDelay = 1.0f;
	
	float gameOverTimer = 0.0f;
	
	void FixedUpdate () 
	{
		shootTimer += Time.deltaTime;
		if (shootTimer >= shootDelay && transform.Find("Display").renderer.enabled)
		{
			shootTimer = 0.0f;
			audio.Play();
			Instantiate(bullet, transform.position, transform.rotation);	
		}
		
		if (!transform.Find("Display").renderer.enabled)
		{
			gameOverTimer += Time.deltaTime;
			if (gameOverTimer >= 1.0f)
				Application.LoadLevel("Menu");
		}
	}
}
