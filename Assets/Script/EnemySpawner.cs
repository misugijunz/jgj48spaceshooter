﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	[SerializeField]
	GameObject[] enemies;
	
	[SerializeField]
	GameObject[] obstacle;
	
	float spawnTimer = 0.0f;
	float spawnDelay = 0.5f;
	int counter = 0;
	
	void Update () 
	{
		spawnTimer += Time.deltaTime;
		if (spawnTimer >= spawnDelay)
		{
			counter++;
			spawnTimer = 0.0f;
			Vector3 enemyPos = new Vector3(Random.Range(-6.0f, 6.0f), 1.0f, 10.0f);
			Instantiate(enemies[Random.Range(0, enemies.Length)], enemyPos, transform.rotation);
			
			enemyPos.y = 0.5f;
			if (counter % 10 == 0)
				Instantiate(obstacle[Random.Range(0, obstacle.Length)], enemyPos, transform.rotation);
		}
	}
}
