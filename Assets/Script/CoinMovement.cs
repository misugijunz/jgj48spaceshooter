﻿using UnityEngine;
using System.Collections;

public class CoinMovement : MonoBehaviour {
	float speed = 5.0f;
	
	void FixedUpdate ()
	{
		transform.Translate(Vector3.back * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag("Player") && other.transform.Find("Display").renderer.enabled)
		{
			PlayerStat.coin += 1;
			Destroy(gameObject);
		}
	}
}
