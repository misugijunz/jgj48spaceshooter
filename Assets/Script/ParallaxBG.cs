using UnityEngine;
using System.Collections;

public class ParallaxBG : MonoBehaviour {
	float offset;
	float scrollSpeed = 10.0f;
	
	void FixedUpdate () 
	{
		offset = Time.fixedTime * scrollSpeed * Time.fixedDeltaTime;
		renderer.material.mainTextureOffset = new Vector2(0.0f, -offset);
	}
}
